<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property int $game
 * @property int $turn
 * @property int $whites
 * @property string $from
 * @property string $to
 * @property string $figure
 * @property string $action
 * @property int $enPassant
 * @property int $checkmate
 */
class Move extends Model
{
    protected $table = 'move';

    const STATUS_CHECK = 1;
    const STATUS_MATE = 2;

    use SoftDeletes;
}
