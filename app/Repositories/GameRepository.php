<?php
namespace App\Repositories;

use App\Move;

class GameRepository
{
    private function getQuery(int $gameId)
    {
        return Move::query()
            ->select(
                'id',
                'turn',
                'whites',
                'from',
                'to',
                'figure',
                'action',
                'enPassant',
                'checkmate',
                'created_at'
            )
            ->where('game', $gameId);
    }

    public function getMovesByGameId(int $gameId)
    {
        $query = $this->getQuery($gameId);

        return $query->get();
    }

    public function getMoveById(int $gameId, int $moveId)
    {
        $query = $this->getQuery($gameId);

        return $query
            ->where('id', $moveId)
            ->first();
    }
}
