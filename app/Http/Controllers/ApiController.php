<?php

namespace App\Http\Controllers;

use App\Game;
use App\Move;
use App\Repositories\GameRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    private $request;
    private $repository;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @param GameRepository $repo
     */
    public function __construct(Request $request, GameRepository $repo)
    {
        $this->request = $request;
        $this->repository = $repo;
    }

    public function create()
    {
        $data = json_decode($this->request->getContent());

        $game = new Game();
        $game->save();
        $gameId = $game->id;

        foreach ($data->data->included as $move) {
            $this->saveMoveData($gameId, new Move(), $move->attributes);
        }

        return response('', 201);
    }

    private function saveMoveData(int $gameId, Move $move, \stdClass $data)
    {
        $move->game = $gameId;
        $move->turn = $data->turn;
        $move->whites = $data->whites ? 1 : 0;
        $move->from = $data->from;
        $move->to = $data->to;
        $move->figure = $data->figure;
        $move->action = $data->action ?? null;
        $move->enPassant = $data->enPassant ? 1 : 0;
        $move->checkmate = $data->check
            ? ($data->mate ? 2 : 1)
            : null;

        $move->save();
    }

    public function getById(int $gameId)
    {
        $moves = $this->repository->getMovesByGameId($gameId);

        $data = [
            'links' => [
                'self' => $this->request->fullUrl(),
            ],
        ];

        if ($moves->count()) {
            $moves = $this->formatMoves($moves);

            $data['data'] = [
                'type' => 'chessGame',
                'id' => $gameId,
            ];
            $data['included'] = $moves;
        } else {
            $data['errors'] = [
                [
                    'code' => 1,
                    'detail' => 'chessGame with id [' . $gameId . '] not found',
                ],
            ];
        }

        return response()->json($data);
    }

    private function formatMoves(Collection $data)
    {
        $moves = [];

        foreach ($data as &$move) {
            $moves[] = [
                'type' => 'chessMove',
                'id' => $move->id,
                'attributes' => [
                    'turn' => $move->turn,
                    'whites' => (bool)$move->whites,
                    'from' => $move->from,
                    'to' => $move->to,
                    'figure' => $move->figure,
                    'action' => $move->action,
                    'enPassant' => (bool)$move->enPassant,
                    'check' => in_array($move->checkmate, [Move::STATUS_CHECK, Move::STATUS_MATE]),
                    'mate' => $move->checkmate === Move::STATUS_MATE,
                    'created' => $move->created_at->toDateTimeString(),
                ]
            ];
        }
        return $moves;
    }

    public function update(int $gameId)
    {
        $input = json_decode($this->request->getContent());

        $move = isset($input->data->id)
            ? $this->repository->getMoveById($gameId, $input->data->id)
            : new Move();

        $this->saveMoveData($gameId, $move, $input->data->attributes);

        return response('', 204);
    }
}
