<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('move', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('game')->comment('id партии из таблицы game');
            $table->integer('turn')->comment('Номер хода, присваивается двум полуходам');
            $table->tinyInteger('whites')->comment('Ход белых: 1-да, 0-нет');
            $table->string('from')->comment('Поле, с которого делается ход');
            $table->string('to')->comment('Поле, на которое делается ход');
            $table->string('figure')->comment('Код фигуры');
            $table->string('action')->nullable()->comment('Действие при ходе (взятие фигуры и пр.)');
            $table->tinyInteger('enPassant')->comment('Взятие пешки на проходе (En Passant): 1-да, 0-нет');
            $table->tinyInteger('checkmate')->nullable()->comment('Шах или мат: null-нет, 1-шах, 2-мат');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('move');
    }
}
