<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//получение шахматной позиции по id
$router->get('/positions/{id}', 'ApiController@getById');

//запись новой позиции
$router->post('/positions', 'ApiController@create');

//изменение существующей позиции
$router->post('/positions/{id}', 'ApiController@update');
